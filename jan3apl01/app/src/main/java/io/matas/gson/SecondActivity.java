package io.matas.gson;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

import com.google.gson.Gson;

import butterknife.BindView;
import butterknife.ButterKnife;


/**
 * Created by Matas Ramasauskas on 03/01/2017.
 * For Mattdevelops LTC.
 * You can contact me at : dev.matas@gmail.com
 * Or see link for more details http://mattdevleops.github.io
 */

public class SecondActivity extends AppCompatActivity {


    @BindView(R.id.secondAge)
    TextView ageField;

    private int getAge(int year){

        return Constants.CURRENT_YEAR - year;

    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.second_activity);
        ButterKnife.bind(this);
        String recievedJSON = getIntent().getExtras().getString(Constants.USER_JSON_KEY);
        User recievedUser = new Gson().fromJson(recievedJSON,User.class);
        ageField.setText(recievedUser.getSchoolName());

    }
}
