package io.matas.gson;

/**
 * Created by Matas Ramasauskas on 03/01/2017.
 * For Mattdevelops LTC.
 * You can contact me at : dev.matas@gmail.com
 * Or see link for more details http://mattdevleops.github.io
 */

public class User {

    private String name;
    private String email;
    private String lastName;
    private String schoolName;
    private String phoneNumber;
    private int birthdayYear;


    public User(String name, String email, String lastName, String phoneNumber, int birthdayYear, String schoolName) {
        this.name = name;
        this.email = email;
        this.lastName = lastName;
        this.phoneNumber = phoneNumber;
        this.birthdayYear = birthdayYear;
        this.schoolName = schoolName;
    }

    public String getSchoolName() {
        return schoolName;
    }

    public void setSchoolName(String schoolName) {
        this.schoolName = schoolName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public int getBirthdayYear() {
        return birthdayYear;
    }

    public void setBirthdayYear(int birthdayYear) {
        this.birthdayYear = birthdayYear;
    }

}
