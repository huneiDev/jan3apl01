package io.matas.gson;

/**
 * Created by Matas Ramasauskas on 03/01/2017.
 * For Mattdevelops LTC.
 * You can contact me at : dev.matas@gmail.com
 * Or see link for more details http://mattdevleops.github.io
 */

public class Constants {

    public static final String USER_JSON_KEY = "newUserJson";
    public static final int CURRENT_YEAR = 2017;


}
