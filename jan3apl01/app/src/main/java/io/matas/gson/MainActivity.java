package io.matas.gson;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.widget.EditText;
import android.widget.TextView;

import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends AppCompatActivity {

    @BindView(R.id.errorField)
    TextView errorField;

    @BindView(R.id.nameField)
    EditText nameField;

    @BindView(R.id.surnameField)
    EditText surnameField;

    @BindView(R.id.emailField)
    EditText emailField;

    @BindView(R.id.phoneField)
    EditText phoneField;

    @BindView(R.id.birthdayYearField)
    EditText birthdayField;

    @BindView(R.id.schoolField)
    EditText schoolField;




    public static boolean isEditTextsEmpty(List<EditText> texts){

        for (int i = 0; i < texts.size(); i++) {


            if(isEditTextEmpty(texts.get(i))){
                return true;
            }


        }

        return false; // Reiskias visi elemtai buvo uzpildyti.

    }

    public static boolean isEditTextEmpty(EditText text){

        if(text != null){

            if(text.getText().toString().isEmpty()){
                return true;
            }

        }

        return false;

    }


    public static String getTextFromField(EditText text){

        return text.getText().toString();

    }

    public static int getIntFromField(EditText text){

        return Integer.valueOf(text.getText().toString());

    }

    @OnClick(R.id.submitButton) void onSubmitClick(){

        List<EditText> fields = new ArrayList<>();
        fields.add(nameField);
        fields.add(phoneField);
        fields.add(surnameField);
        fields.add(schoolField);
        fields.add(emailField);
        fields.add(birthdayField);


        if(isEditTextsEmpty(fields)){

            errorField.setText("Please fill all fields");
            return; // Iseiname is funkcijos, toliau nieko nebevykdo.
        }

        Handler handler = new Handler();
        final User user = new User(getTextFromField(nameField),getTextFromField(emailField),getTextFromField(surnameField),getTextFromField(phoneField),getIntFromField(birthdayField),getTextFromField(schoolField)); // Sukuriame objekta

        handler.postDelayed(new Runnable() {
            @Override
            public void run() {

                enterNewActivity(user);

            }
        },5000);


    }

    void enterNewActivity(User user){

        if(user != null){ // Isvengiam "Null reference exception"

            String json = new Gson().toJson(user, User.class);
            Intent newIntent = new Intent(this,SecondActivity.class);
            newIntent.putExtra(Constants.USER_JSON_KEY,json);
            startActivity(newIntent);
        }

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);



    }
}
